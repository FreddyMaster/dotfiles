## Install Guide:
### Simply run:
```shell
git clone https://gitlab.com/FreddyMaster/dotfiles.git
cd dotfiles
./install.sh
```
#### Note: If it will not let you execute "install.sh" run:
```shell
chmod +x install.sh
```

## Screenshot
![Screenshot of my rice](https://gitlab.com/FreddyMaster/dotfiles/-/raw/master/Pictures/screenshot.png)
