#!/bin/sh
set -e

echo "Welcome!" && sleep 2

# aliases

# does full system update
echo "Doing a system update, cause stuff may break if it's not the latest version..."
sudo pacman --noconfirm -Syyu

echo "###########################################################################"
echo "Will do stuff, get ready"
echo "###########################################################################"

# install base-devel if not installed
sudo pacman -S --noconfirm --needed base-devel wget

# choose video driver
echo "1) xf86-video-intel 	2) xf86-video-amdgpu 3) Skip"
read -r -p "Choose you video card driver(default 1)(will not re-install): " vid

case $vid in 
[1])
	DRI='xf86-video-intel'
	;;

[2])
	DRI='xf86-video-amdgpu'
	;;
[3])
	DRI=""
	;;
[*])
	DRI='xf86-video-intel'
	;;
esac

# install xorg if not installed and all other stuff
sudo pacman -S --noconfirm --needed feh xorg xorg-xinit xorg-xkill xorg-xinput $DRI xmonad zsh dunst kitty mpd neovim vifm ncmpcpp nitrogen ttf-font-awesome mpc curl jq imagemagick libnotify noto-fonts-extra noto-fonts gnu-free-fonts font-bh-ttf sdl2_ttf ttf-bitstream-vera ttf-dejavu opendesktop-fonts

# install mpnotd
git clone https://github.com/jeffmhubbard/mpnotd-zsh
cd mpnotd-zsh
sudo make install


# install fonts, window manager and terminal
mkdir -p ~/.local/share/fonts
mkdir -p ~/.src

# install yay
read -r -p "Would you like to install yay? Say no if you already have it (step necessary because well, we need some stuff) [yes/no]: " yay
# echo "Please replace libxft with libxft-bgra in next install" 
sleep 3

case $yay in
[yY][eE][sS]|[yY])
	git clone https://aur.archlinux.org/yay.git ~/.src/yay
	(cd ~/.src/yay/ && makepkg -si )
	yay -S picom-jonaburg-git xmonad-contrib xmenu xmenud-git xclickroot-git ttf-material-design-icons-git ttf-material-icons-git ttf-font-awesome-4 networkmanager-dmenu-git nerd-fonts-ubuntu-mono nerd-fonts-mononoki ttf-ms-fonts
	;;

[nN][oO]|[nN])
	echo "Installing Other Stuff then"
	yay -S picom-jonaburg-git xmonad-contrib xmenu xmenud-git xclickroot-git ttf-material-design-icons-git ttf-material-icons-git ttf-font-awesome-4 networkmanager-dmenu-git nerd-fonts-ubuntu-mono nerd-fonts-mononoki ttf-ms-fonts
	;;

[*])
	echo "Lets do it anyways lol" 
	yay -S picom-jonaburg-git xmonad-contrib xmenu xmenud-git xclickroot-git ttf-material-design-icons-git ttf-material-icons-git ttf-font-awesome-4 networkmanager-dmenu-git nerd-fonts-ubuntu-mono nerd-fonts-mononoki ttf-ms-fonts
	sleep 1
	;;
esac

#install custom picom config file 
mkdir -p ~/.config/
# cd .config/
# git clone https://gist.github.com/f70dea1449cfae856d42b771912985f9.git ./picom 
    if [ -f ~/.zshrc ]; then
        echo "Zsh config detected, backing up..."
        cp ~/.zshrc ~/.zhsrc.old;
        cp ./.zshrc ~/*;
    else
        echo "Installing bash configs..."
        cp ./.zshrc ~/*;
    fi
    if [ -f ~/.bashrc ]; then
        echo "Zsh config detected, backing up..."
        cp ~/.bashrc ~/.bashrc.old;
        cp ./.bashrc ~/*;
    else
        echo "Installing bash configs..."
        cp ./.bashrc ~/*;
    fi
    if [ -d ~/.config/dmenu ]; then
        echo "Dmenu configs detected, backing up..."
        mkdir ~/.config/dmenu.old && mv ~/.config/dmenu/* ~/.config/dmenu.old/;
        cp -r ./.config/dmenu/* ~/.config/dmenu;
    else
        echo "Installing dmenu configs..."
        mkdir ~/.config/dmenu && cp -r ./config/dmenu/* ~/.config/dmenu;
	fi
    if [ -d ~/.config/mpv ]; then
        echo "mpv configs detected, backing up..."
        mkdir ~/.config/mpv.old && mv ~/.config/mpv/* ~/.config/mpv.old/;
        cp -r ./.config/mpv/* ~/.config/mpv;
    else
        echo "Installing mpv configs..."
        mkdir ~/.config/mpv && cp -r ./config/mpv/* ~/.config/mpv;
    fi
    if [ -d ~/.config/networkmanager-dmenu ]; then
        echo "networkmanager-dmenu configs detected, backing up..."
        mkdir ~/.config/networkmanager-dmenu.old && mv ~/.config/networkmanager-dmenu/* ~/.config/networkmanager-dmenu.old/;
        cp -r ./.config/networkmanager-dmenu/* ~/.config/networkmanager-dmenu;
    else
        echo "Installing networkmanager-dmenu configs..."
        mkdir ~/.config/networkmanager-dmenu && cp -r ./config/networkmanager-dmenu/* ~/.config/networkmanager-dmenu;
	fi
    if [ -d ~/.config/picom ]; then
        echo "Picom configs detected, backing up..."
        cp -r ~/.config/picom/* ~/.config/picom.old/;
        cp -r ./.config/picom/* ~/.config/picom/;
    else
        echo "Installing picom configs..."
         mkdir ~/.config/picom/ && cp -r ./.config/picom/* ~/.config/picom/;
    fi
    if [ -d ~/.config/nitrogen ]; then
        echo "nitrogen configs detected, backing up..."
        cp -r ~/.config/nitrogen/* ~/.config/nitrogen.old/;
        cp -r ./.config/nitrogen/* ~/.config/nitrogen/;
    else
        echo "Installing nitrogen configs..."
         mkdir ~/.config/nitrogen/ && cp -r ./.config/nitrogen/* ~/.config/nitrogen/;
    fi
    if [ -d ~/.config/nvim ]; then
        echo "neovim configs detected, backing up..."
        cp -r ~/.config/nvim/* ~/.config/nvim.old/;
        cp -r ./.config/nvim/* ~/.config/nvim/;
    else
        echo "Installing neovim configs..."
         mkdir ~/.config/nvim/ && cp -r ./.config/nvim/* ~/.config/nvim/;
    fi
    if [ -d ~/.config/vifm ]; then
        echo "vifm configs detected, backing up..."
        cp -r ~/.config/vifm/* ~/.config/vifm.old/;
        cp -r ./.config/vifm/* ~/.config/vifm/;
    else
        echo "Installing vifm configs..."
         mkdir ~/.config/vifm/ && cp -r ./.config/vifm/* ~/.config/vifm/;
    fi
        if [ -d ~/.config/xmenu ]; then
        echo "xmenu configs detected, backing up..."
        cp -r ~/.config/xmenu/* ~/.config/xmenu.old/;
        cp -r ./.config/xmenu/* ~/.config/xmenu/;
    else
        echo "Installing xmenu configs..."
         mkdir ~/.config/xmenu/ && cp -r ./.config/xmenu/* ~/.config/xmenu/;
    fi
    if [ -d ~/.config/xclickroot ]; then
        echo "xclickroot configs detected, backing up..."
        cp -r ~/.config/xclickroot/* ~/.config/xclickroot.old/;
        cp -r ./.config/xclickroot/* ~/.config/xclickroot/;
    else
        echo "Installing xclickroot configs..."
         mkdir ~/.config/xclickroot/ && cp -r ./.config/xclickroot/* ~/.config/xclickroot/;
    fi
    if [ -d ~/.config/kitty ]; then
        echo "Kitty configs detected, backing up..."
        cp -r ~/.config/kitty ~/.config/kitty.old;
        cp -r ./config/kitty ~/.config/kitty;
    else
        echo "Installing kitty configs..."
         cp -r ./config/kitty ~/.config/kitty;
	fi
    if [ -d ~/.config/dunst ]; then
        echo "Dunst configs detected, backing up..."
        mkdir ~/.config/dunst.old && mv ~/.config/dunst/* ~/.config/dunst.old/
        cp -r ./config/dunst/* ~/.confg/dunst;
    else
        echo "Installing dunst configs..."
        mkdir ~/.config/dunst && cp -r ./config/dunst/* ~/.config/dunst;
    fi
    if [ -d ~/.wallpapers ]; then
        echo "Adding wallpaper to ~/.wallpapers..."
        cp -r ./.wallpapers/ ~/.wallpapers/;
    else
        echo "Installing wallpaper..."
        mkdir ~/.wallpapers && cp -r ./.wallpapers/* ~/.wallpapers/;
	fi
    if [ -d ~/.config/polybar ]; then
        echo "Polybar configs detected, backing up..."
        mkdir ~/.config/polybar.old && mv ~/.config/polybar/* ~/.config/polybar.old/
        cp -r ./config/polybar/* ~/.config/polybar;
	else
        echo "Installing polybar configs..."
        mkdir ~/.config/polybar && cp -r ./config/polybar/* ~/.config/polybar;
    fi
    if [ -d ~/.config/X11 ]; then
        echo "X11 configs detected, backing up..."
        mkdir ~/.config/X11.old && mv ~/.config/X11/* ~/.config/X11.old/
        cp -r ./config/X11/* ~/.config/X11;
	else
        echo "Installing X11 configs..."
        mkdir ~/.config/X11 && cp -r ./config/X11/* ~/.config/X11;
    fi
    if [ -d ~/.config/mpd ]; then
        echo "mpd configs detected, backing up..."
        mkdir ~/.config/mpd.old && mv ~/.config/mpd/* ~/.config/mpd.old/
        cp -r ./config/mpd/* ~/.config/mpd;
	else
        echo "Installing mpd configs..."
        mkdir ~/.config/mpd && cp -r ./config/mpd/* ~/.config/mpd;
    fi
    if [ -d ~/.config/ncmpcpp ]; then
        echo "ncmpcpp configs detected, backing up..."
        mkdir ~/.config/ncmpcpp.old && mv ~/.config/ncmpcpp/* ~/.config/ncmpcpp.old/
        cp -r ./config/ncmpcpp/* ~/.config/ncmpcpp;
	else
        echo "Installing ncmpcpp configs..."
        mkdir ~/.config/ncmpcpp && cp -r ./config/ncmpcpp/* ~/.config/ncmpcpp;
    fi
    if [ -d ~/.xmonad ]; then
        echo "XMonad configs detected, backing up..."
        mkdir ~/.xmonad.old && mv ~/.xmonad/* ~/.xmonad.old/
        cp -r ./xmonad/* ~/.xmonad/;
    else
        echo "Installing xmonad configs..."
        mkdir ~/.xmonad && cp -r ./xmonad/* ~/.xmonad;
    fi
    if [ -d ~/.scripts ]; then
        echo ".scripts directory detected, backing up..."
        mkdir ~/.scripts.old && mv ~/.scripts/* ~/.scripts.old/
        cp -r ./.scripts/* ~/.scripts/;
    else
        echo "Installing .scripts directory..."
        mkdir ~/.scripts && cp -r ./.scripts/* ~/.scripts;
    fi
    if [ -d ~/.config/mpnotd ]; then
        echo "mpnotd configs detected, backing up..."
        mkdir ~/.config/mpnotd.old && mv ~/.config/mpnotd/* ~/.config/mpnotd.old/
        cp -r ./.config/mpnotd/* ~/.config/mpnotd/;
    else
        echo "Installing mpnotd configs..."
        mkdir ~/.config/mpnotd && cp -r ./.config/mpnotd/* ~/.config/mpnotd;
    fi    

# done 
echo "PLEASE MAKE .xinitrc TO LAUNCH, or just use your dm" | tee ~/Note.txt
echo "run 'p10k configure' to set up your zsh" | tee -a ~/Note.txt
echo "after you this -> 'git clone --depth=1 https://github.com/romkatv/powerlevel10k.git \${ZSH_CUSTOM:-\$HOME/.oh-my-zsh/custom}/themes/powerlevel10k'" | tee -a ~/Note.txt
printf "\n" >> ~/Note.txt
echo "Please add: export PATH='\$PATH:/home/{Your_user}/bin' to your .zshrc if not done already. Replace {Your_user} with your username." | tee -a ~/Note.txt
echo "For startpage, import in via nighttab" | tee -a ~/Note.txt
echo "ALL DONE! Issue 'xmonad --recompile' and then re-login for all changes to take place!" | tee -a ~/Note.txt
echo "Make sure your default shell is ZSH too..." | tee -a ~/Note.txt
echo "Open issues on gitlab whatever if you face issues." | tee -a ~/Note.txt
echo "Install Font Awesome" | tee -a ~/Note.txt
echo "If the bar doesn't work, got to the polybar github page, if you're hopelessly lost open an issue." | tee -a ~/Note.txt
echo "These instructions have been saved to ~/Note.txt. Make sure to go through them."
sleep 5
xmonad --recompile


cd ~/

# install zsh and make it default
# sudo pacman --noconfirm --needed -S zsh
#OhMyZsh
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"i
