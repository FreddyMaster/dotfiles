# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH
export PATH="$HOME/.local/bin:$HOME/.cargo/bin:$PATH"

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

export EDITOR="nvim"
export BROWSER="firefox"
# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="powerlevel10k/powerlevel10k"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(zsh-autosuggestions zsh-completions zsh-syntax-highlighting)
autoload -U compinit && compinit
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=8'
setopt INC_APPEND_HISTORY
source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Aliases
#alias ls="colorls --sd -A"
eval $(thefuck --alias)
PRIV="sudo"

alias q="exit"
alias ls="exa -al --color=always --group-directories-first"
alias hd="hexdump -C"
alias default-apps="clear && ~/.scripts/default-apps/launch && ~/.scripts/default-apps/launch -e"
alias failreset="faillock --reset --user aj"
alias menu="sh ~/.config/xmenu/xmenu.sh &"
alias rmorphans="$PRIV  pacman -Qtdq | sudo pacman -Rns -"
alias vim="nvim"
alias refram="$PRIV bash -c \"sync; echo 3 > /proc/sys/vm/drop_caches\""
alias megathread='/usr/bin/git --git-dir=$HOME/Debug-Duckers-Megathread/ --work-tree=$HOME'
alias config='/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME'
alias music="ncmpcpp"
alias ..="cd .."
alias vifm="sh ~/.config/vifm/scripts/vifmrun"
alias ytdl='sh ~/.scripts/ytdl.sh'
#alias yay='paru'
alias yayu='yay -Syyu'
alias screenlayout='sh ~/.screenlayout/xrandr.sh'
alias doomsync='.emacs.d/bin/doom sync'
alias dl196='python3 -m bdfr download /Storage/Memes --subreddit 196 -L 200 --no-dupes --file-scheme "{TITLE}-{POSTID}"'
alias dlsocietylounge='python3 -m bdfr download /Storage/Memes --subreddit societylounge -L 200 --no-dupes --file-scheme "{TITLE}-{POSTID}"'
alias dlshitposting='python3 -m bdfr download /Storage/Memes --subreddit shitposting -L 200 --no-dupes --file-scheme "{TITLE}-{POSTID}"'
alias dlcringe='python3 -m bdfr download /Storage/Memes --subreddit cringetopiarm -L 200 --no-dupes --file-scheme "{TITLE}-{POSTID}"'
alias dldankvideos='python3 -m bdfr download /Storage/Memes --subreddit dankvideos -L 200 --no-dupes --file-scheme "{TITLE}-{POSTID}"'
alias dlokbr='python3 -m bdfr download /Storage/Memes --subreddit okbuddyretard -L 200 --no-dupes --file-scheme "{TITLE}-{POSTID}"'
alias dlthotti='yt-dlp -o /Storage/Memes/ThottiYT/"%(title)s.%(ext)s" https://www.youtube.com/c/Thotti/videos'
alias dlsolid='yt-dlp -o /Storage/Memes/Solidjj/"%(title)s.%(ext)s" https://www.youtube.com/c/Solidjj/videos'
alias dlall='dl196 && dlsocietylounge && dlshitposting && dlcringe && dldankvideos && dlokbr && dlthotti && dlsolid'
alias compress='sh ~/.scripts/compress.sh'
alias bloat='sh ~/.scripts/bloat.sh'
alias mp4togif='sh ~/.scripts/gif.sh'
alias rm='trash -r '
alias discord='discord --no-sandbox'
alias cat='bat'
alias du='dust'
alias cp='rclone copy -P'
alias mv='rclone move -P'
alias night='sh ~/.scripts/night.sh'
alias morning='sh ~/.scripts/morning.sh'
#alias mpv='mpv --shuffle'
alias gccrun='sh ~/.scripts/gccrun.sh'

# GPG Dialog
export GPG_TTY=$(tty)

# To customize prompt, run `p10k configure` or edit ~/.config/zsh/.p10k.zsh.
[[ ! -f ~/.config/zsh/.p10k.zsh ]] || source ~/.config/zsh/.p10k.zsh
