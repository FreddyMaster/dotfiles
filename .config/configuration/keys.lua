-- keys.lua
-- Contains Global Keys
local gears = require("gears")
local awful = require("awful")
local beautiful = require("beautiful")
local hotkeys_popup = require("awful.hotkeys_popup")
local helpers = require("helpers")
-- Custom modules
local machi = require("module.layout-machi")
local bling = require("module.bling")
local switcher = require("module.awesome-switcher")

-- Mouse Bindings
awful.mouse.append_global_mousebindings({
    awful.button({}, 4, awful.tag.viewprev),
    awful.button({}, 5, awful.tag.viewnext)
})

-- Client and Tabs Bindings
awful.keyboard.append_global_keybindings(
    {
        awful.key({"Mod1"}, "s", function()
            bling.module.tabbed.iter()
        end, {description = "Iterate Through Tabbing Group", group = "tabs"}),
        awful.key({"Mod1"}, "d", function() bling.module.tabbed.pop() end, {
            description = "Remove Focused Client From Tabbing Group",
            group = "tabs"
        }), awful.key({modkey}, "Down", function()
            awful.client.focus.bydirection("down")
            bling.module.flash_focus.flashfocus(client.focus)
        end, {description = "Focus Down", group = "client"}),
        awful.key({modkey}, "Up", function()
            awful.client.focus.bydirection("up")
            bling.module.flash_focus.flashfocus(client.focus)
        end, {description = "Focus Up", group = "client"}),
        awful.key({modkey}, "Left", function()
            awful.client.focus.bydirection("left")
            bling.module.flash_focus.flashfocus(client.focus)
        end, {description = "Focus Left", group = "client"}),
        awful.key({modkey}, "Right", function()
            awful.client.focus.bydirection("right")
            bling.module.flash_focus.flashfocus(client.focus)
        end, {description = "Focus Right", group = "client"}),
        awful.key({modkey}, "j", function() awful.client.focus.byidx(1) end,
                  {description = "Focus Next By Index", group = "client"}),
        awful.key({modkey}, "k", function() awful.client.focus.byidx(-1) end,
                  {description = "Focus Previous By Index", group = "client"}),
        awful.key({modkey, "Shift"}, "j",
                  function() awful.client.swap.byidx(1) end, {
            description = "Swap With Next Client By Index",
            group = "client"
        }),
        awful.key({modkey, "Shift"}, "k",
                  function() awful.client.swap.byidx(-1) end, {
            description = "Swap With Previous Client By Index",
            group = "client"
        }),
        awful.key({ "Mod1",           }, "Tab",
                  function() switcher.switch( 1, "Mod1", "Alt_L", "Shift", "Tab") end, {
            description = "Alt-tab", group = "client"}
        ),
        awful.key({ "Mod1", "Shift"   }, "Tab",
                  function() switcher.switch(-1, "Mod1", "Alt_L", "Shift", "Tab") end, {
            description = "Go Back", group = "client"})
    })

-- Awesomewm
awful.keyboard.append_global_keybindings(
    {
        -- Volume control
        awful.key({}, "XF86AudioRaiseVolume",
                  function() awful.spawn("pamixer -i 3") end,
                  {description = "Increase Volume by 3", group = "awesome"}),
        awful.key({}, "XF86AudioLowerVolume",
                  function() awful.spawn("pamixer -d 3") end,
                  {description = "Decrease Volume by 3", group = "awesome"}),
        awful.key({}, "XF86AudioMute", function()
            awful.spawn("pamixer -t")
        end, {description = "Mute Volume", group = "awesome"}), -- Media Control
        awful.key({}, "XF86AudioPlay",
                  function() awful.spawn("playerctl play-pause") end,
                  {description = "Toggle Playerctl", group = "awesome"}),
        awful.key({}, "XF86AudioPrev",
                  function() awful.spawn("playerctl previous") end,
                  {description = "Playerctl Previous", group = "awesome"}),
        awful.key({}, "XF86AudioNext",
                  function() awful.spawn("playerctl next") end,
                  {description = "Playerctl Next", group = "awesome"}),

        -- Screen Shots/Vids
        awful.key({}, "Print",
          function() awful.spawn("flameshot gui") end,
             {description = "Take a Screenshot With Flameshot", group = "awesome"}),
        awful.key({modkey}, "Print",
          function() awful.spawn("flameshot launcher") end,
             {description = "Open Screenshot Launcher", group = "awesome"}),

        -- Brightness
        awful.key({}, "XF86MonBrightnessUp",
                  function() awful.spawn("xbacklight -inc 5") end,
                  {description = "Increase Brightness", group = "awesome"}),
        awful.key({}, "XF86MonBrightnessDown",
                  function() awful.spawn("xbacklight -dec 5") end,
                  {description = "Decrease Brightness", group = "awesome"}),

        -- Awesome stuff
        awful.key({modkey}, "F1", hotkeys_popup.show_help,
                  {description = "Show Help", group = "awesome"}),
        awful.key({modkey}, "Escape", awful.tag.history.restore,
                  {description = "Go Back", group = "tag"}),
        awful.key({modkey, shift}, "d", function()
            awesome.emit_signal("widgets::start::toggle", mouse.screen)
        end, {description = "Show Panel", group = "awesome"}),
        awful.key({modkey}, "x", function()
            awesome.emit_signal("widgets::exit_screen::toggle")
        end, {description = "Show Exit Screen", group = "awesome"}),
        awful.key({modkey}, "q", awesome.restart,
                  {description = "Reload Awesome", group = "awesome"}),
        awful.key({modkey, "Shift"}, "q", awesome.quit,
                  {description = "Quit Awesome", group = "awesome"})

    })

-- Launcher and screen
awful.keyboard.append_global_keybindings(
    {
        awful.key({modkey, "Control"}, "j",
                  function() awful.screen.focus_relative(1) end,
                  {description = "Focus The Next Screen", group = "screen"}),
        awful.key({modkey, "Control"}, "k",
                  function() awful.screen.focus_relative(-1) end,
                  {description = "Focus The Previous Screen", group = "screen"}),

        -- Dmenu
        awful.key({modkey}, "p", function() awful.spawn(launcher) end,
                  {description = "Run Dmenu", group = "launcher"}),
        -- Emacs
        awful.key({modkey}, "e", function() awful.spawn("emacsclient -c -a 'emacs'") end,
                  {description = "Open Emacs", group = "launcher"}),
        -- Steam
        awful.key({modkey}, "g", function() awful.spawn("steam-native") end,
                  {description = "Open Emacs", group = "launcher"}),

        -- Terminal
        awful.key({modkey}, "Return", function() awful.spawn(terminal) end,
                  {description = "Open A Terminal", group = "launcher"}),

        awful.key({modkey}, "s",
                  function() awesome.emit_signal("scratch::music") end,
                  {description = "Open Ncmpcpp", group = "scratchpad"}),

        awful.key({modkey}, "f", function() awful.spawn(filemanager) end,
                  {description = "Open File Browser", group = "launcher"}),
        awful.key({modkey}, "v",
                  function() awesome.emit_signal("scratch::term") end,
                  {description = "Open Scratchpad Terminal", group = "scratchpad"}),
        awful.key({modkey}, "w", function()
            awful.spawn.with_shell(browser)
        end, {description = "Open Browser", group = "launcher"}),

        awful.key({modkey}, "l", function() awful.tag.incmwfact(0.05) end, {
            description = "Increase Master Width Factor",
            group = "layout"
        }), awful.key({modkey}, "h", function()
            awful.tag.incmwfact(-0.05)
        end, {description = "Decrease Master Width Factor", group = "layout"}),
        awful.key({modkey, "Shift"}, "h",
                  function() awful.tag.incnmaster(1, nil, true) end, {
            description = "Increase The Number Of Master Clients",
            group = "layout"
        }), awful.key({modkey, "Shift"}, "l",
                      function() awful.tag.incnmaster(-1, nil, true) end, {
            description = "Decrease The Number Of Master Clients",
            group = "layout"
        }), awful.key({modkey, "Control"}, "h",
                      function() awful.tag.incncol(1, nil, true) end, {
            description = "Increase The Number Of Columns",
            group = "layout"
        }), awful.key({modkey, "Control"}, "l",
                      function() awful.tag.incncol(-1, nil, true) end, {
            description = "Decrease The Number Of Columns",
            group = "layout"
        }), awful.key({modkey}, "space", function() awful.layout.inc(1) end,
                      {description = "Select Next Layout", group = "layout"}),
        awful.key({modkey, "Shift"}, "space",
                  function() awful.layout.inc(-1) end,
                  {description = "Select Previous Layout", group = "layout"}), -- Set Layout
        awful.key({modkey, "Control"}, "w",
                  function() awful.layout.set(awful.layout.suit.max) end,
                  {description = "Set Max Layout", group = "tag"}),
        awful.key({modkey}, "s",

                  function() awful.layout.set(awful.layout.suit.tile) end,
                  {description = "Set Tile Layout", group = "tag"}),
        awful.key({modkey, shift}, "s",
                  function() awful.layout.set(awful.layout.suit.floating) end,
                  {description = "Set Floating Layout", group = "tag"}),

        awful.key({modkey, "Control"}, "n", function()
            local c = awful.client.restore()
            -- Focus restored client
            if c then
                c:emit_signal("request::activate", "key.unminimize",
                              {raise = true})
            end
        end, {description = "Restore Minimized", group = "client"})
    })

-- Client management keybinds
client.connect_signal("request::default_keybindings", function()
    awful.keyboard.append_client_keybindings(
        {
            awful.key({modkey, "Shift"}, "f", function(c)
                c.fullscreen = not c.fullscreen
                c:raise()
            end, {description = "Toggle Fullscreen", group = "client"}),
            awful.key({modkey, "Shift"}, "c", function(c) c:kill() end,
                      {description = "Close", group = "client"}),
            awful.key({modkey, "Control"}, "space",
                      awful.client.floating.toggle,
                      {description = "Toggle Floating", group = "client"}),
            awful.key({modkey, "Control"}, "Return",
                      function(c) c:swap(awful.client.getmaster()) end,
                      {description = "Move To Master", group = "client"}),
            awful.key({modkey}, "o", function(c) c:move_to_screen() end,
                      {description = "Move To Screen", group = "client"}),
            --  awful.key({ modkey, "Shift"   }, "t",      function (c) c.ontop = not c.ontop            end,
            --            {description = "toggle keep on top", group = "client"}),
            awful.key({modkey, shift}, "b", function(c)
                c.floating = not c.floating
                c.width = 400
                c.height = 200
                awful.placement.bottom_right(c)
                c.sticky = not c.sticky
            end, {description = "toggle keep on top", group = "client"}),

            awful.key({modkey}, "n", function(c)
                -- The client currently has the input focus, so it cannot be
                -- minimized, since minimized clients can't have the focus.
                c.minimized = true
            end, {description = "Minimize", group = "client"}),
            awful.key({modkey}, "m", function(c)
                c.maximized = not c.maximized
                c:raise()
            end, {description = "(Un)Maximize", group = "client"}),
            awful.key({modkey, "Control"}, "m", function(c)
                c.maximized_vertical = not c.maximized_vertical
                c:raise()
            end, {description = "(Un)Maximize Vertically", group = "client"}),
            awful.key({modkey, "Shift"}, "m", function(c)
                c.maximized_horizontal = not c.maximized_horizontal
                c:raise()
            end, {description = "(Un)Maximize Horizontally", group = "client"}),

            -- On the fly useless gaps change
            awful.key({modkey}, "=", function()
                helpers.resize_gaps(5)
            end),
            awful.key({modkey}, "-", function()
                helpers.resize_gaps(-5)
            end), -- Single tap: Center client 
            -- Double tap: Center client + Floating + Resize
            awful.key({modkey}, "c", function(c)
                awful.placement.centered(c, {
                    honor_workarea = true,
                    honor_padding = true
                })
                helpers.single_double_tap(nil, function()
                    helpers.float_and_resize(c, screen_width * 0.25,
                                             screen_height * 0.28)
                end)
            end)
        })
end)

-- Num row keybinds
awful.keyboard.append_global_keybindings(
    {
        awful.key {
            modifiers = {modkey},
            keygroup = "numrow",
            description = "Only View Tag",
            group = "tag",
            on_press = function(index)
                local screen = awful.screen.focused()
                local tag = screen.tags[index]
                if tag then tag:view_only() end
            end
        }, awful.key {
            modifiers = {modkey, "Control"},
            keygroup = "numrow",
            description = "Toggle Tag",
            group = "tag",
            on_press = function(index)
                local screen = awful.screen.focused()
                local tag = screen.tags[index]
                if tag then awful.tag.viewtoggle(tag) end
            end
        }, awful.key {
            modifiers = {modkey, "Shift"},
            keygroup = "numrow",
            description = "Move Focused Client To Tag",
            group = "tag",
            on_press = function(index)
                if client.focus then
                    local tag = client.focus.screen.tags[index]
                    if tag then client.focus:move_to_tag(tag) end
                end
            end
        }, awful.key {
            modifiers = {modkey, "Control", "Shift"},
            keygroup = "numrow",
            description = "Toggle Focused Client On Tag",
            group = "tag",
            on_press = function(index)
                if client.focus then
                    local tag = client.focus.screen.tags[index]
                    if tag then client.focus:toggle_tag(tag) end
                end
            end
        }, awful.key {
            modifiers = {modkey},
            keygroup = "numpad",
            description = "Select Layout Directly",
            group = "layout",
            on_press = function(index)
                local t = awful.screen.focused().selected_tag
                if t then t.layout = t.layouts[index] or t.layout end
            end
        }
    })

client.connect_signal("request::default_mousebindings", function()
    awful.mouse.append_client_mousebindings(
        {
            awful.button({}, 1,
                         function(c)
                c:activate{context = "mouse_click"}
            end), awful.button({modkey}, 1, function(c)
                c:activate{context = "mouse_click", action = "mouse_move"}
            end), awful.button({modkey}, 3, function(c)
                c:activate{context = "mouse_click", action = "mouse_resize"}
            end)
        })
end)

-- EOF ------------------------------------------------------------------------
