-- autostart.lua
-- Autostart Stuff Here
local awful = require("awful")
local gears = require("gears")

local function run_once(cmd)
    local findme = cmd
    local firstspace = cmd:find(' ')
    if firstspace then findme = cmd:sub(0, firstspace - 1) end
    awful.spawn.easy_async_with_shell(string.format(
                                          'pgrep -u $USER -x %s > /dev/null || (%s)',
                                          findme, cmd))
end

-- LuaFormatter off
-- Add apps to autostart here
autostart_apps = {
    -- Nitrogen
    "nitrogen --restore",
    -- Disk Manager Applet
    "udiskie -a -t -N &",
    -- Redshift
    "redshift-gtk &",
    -- Discord
    "discord &",
    -- Mpd
    "mpd & ",
    -- MpDris2
    -- "MpDris2 &",
    -- Flameshot
    "flameshot &",
    -- nm-applet
    "nm-applet &",
    -- Compositor
    "picom --config ~/.config/picom/picom.conf &",
    -- Media controller daemon
    -- "playerctld daemon &",
    -- xfce polkit
    "/usr/lib/mate-polkit/polkit-mate-authentication-agent-1 &",
    -- Emacs Daemon
    "emacs --daemon &",
    -- Screenlayout
    "sh ~/.screenlayout/xrandr.sh &"
}
-- LuaFormatter on

for app = 1, #autostart_apps do run_once(autostart_apps[app]) end

-- EOF ------------------------------------------------------------------------
