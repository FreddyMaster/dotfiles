-- wibar.lua
-- Wibar (top bar)
local awful = require("awful")
local gears = require("gears")
local gfs = require("gears.filesystem")
local wibox = require("wibox")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local helpers = require("helpers")
local menubar = require("menubar")
local calendar_widget = require("awesome-wm-widgets.calendar-widget.calendar")
local weather_widget = require("awesome-wm-widgets.weather-widget.weather")

local systray_margin = (beautiful.wibar_height - beautiful.systray_icon_size) /
                           2

-- Awesome Panel -----------------------------------------------------------

local icon1 = wibox.widget {
    widget = wibox.widget.imagebox,
    image = gears.surface.load_uncached(gfs.get_configuration_dir() ..
                                            "icons/ghosts/awesome.png"),
    resize = true
}

local awesome_icon = wibox.widget {
    {
        icon1,
        top = dpi(5),
        bottom = dpi(5),
        left = dpi(10),
        right = dpi(5),
        widget = wibox.container.margin
    },
    bg = beautiful.xcolor0,
    widget = wibox.container.background
}

awesome_icon:buttons(gears.table.join(awful.button({}, 1, function()
    awesome.emit_signal("widgets::start::toggle", mouse.screen)
end)))

--[[ awesome.connect_signal("widgets::start::status", function(status)
    if not status then
        icon1.image = unclicked
    else
        icon1.image = clicked
    end
end)
--]]
-- Window Name ----------------------------------------------------------------

-- Date Widget ----------------------------------------------------------------

local date_text = wibox.widget {
  font = beautiful.font_name .. "12",
    format = "%l:%M %p",
    align = 'center',
    valign = 'center',
    widget = wibox.widget.textclock
}

date_text.markup = "<span foreground='" .. beautiful.xcolor11 .. "'>" ..
                       date_text.text .. "</span>"

date_text:connect_signal("widget::redraw_needed", function()
    date_text.markup = "<span foreground='" .. beautiful.xcolor11 .. "'>" ..
                           date_text.text .. "</span>"
end)

local date_icon = wibox.widget {
    font = beautiful.font_name .. "12",
    markup = "<span foreground='" .. beautiful.xcolor11 .. "'>󰥔 </span>",
    align = 'center',
    valign = 'center',
    widget = wibox.widget.textbox
}

local date_pill = wibox.widget {
    {
        date_icon,
        {date_text, top = dpi(1), widget = wibox.container.margin},
        layout = wibox.layout.fixed.horizontal
    },
    left = dpi(10),
    right = dpi(10),
    widget = wibox.container.margin
}
local cw = calendar_widget({
    theme = 'nord',
    placement = 'top_right',
    radius = 5
})

date_pill:connect_signal("button::press",
    function(_, _, _, button)
        if button == 1 then cw.toggle() end
end)

-- Weather---------------------------------------------------------------------
 local weather = weather_widget({
             api_key='4fd610890d8899c5d0082c378b6eeadb',
             coordinates = {42.2645, -71.8041},
             time_format_12h = true,
             units = 'imperial',
             icon_pack_name = 'weather-underground-icons',
             icons_extension = '.svg',
             both_units_widget = true,
             show_hourly_forecast = true,
             show_daily_forecast = true,
             timeout = '120'
         })
local weather_pill = wibox.widget {
    {
        helpers.horizontal_pad(5),
        {weather, top = dpi(1), widget = wibox.container.margin},
        layout = wibox.layout.fixed.horizontal
    },
    left = dpi(8),
    right = dpi(8),
    widget = wibox.container.margin
}

-- Systray Widget -------------------------------------------------------------

local mysystray = wibox.widget.systray()
mysystray:set_base_size(beautiful.systray_icon_size)

local mysystray_container = {
    mysystray,
    left = dpi(8),
    right = dpi(8),
    widget = wibox.container.margin
}

-- Tasklist Buttons -----------------------------------------------------------

local tasklist_buttons = gears.table.join(
                             awful.button({}, 1, function(c)
        if c == client.focus then
            c.minimized = true
        else
            c:emit_signal("request::activate", "tasklist", {raise = true})
        end
    end), awful.button({}, 3, function()
        awful.menu.client_list({theme = {width = 250}})
    end), awful.button({}, 4, function() awful.client.focus.byidx(1) end),
                             awful.button({}, 5, function()
        awful.client.focus.byidx(-1)
    end))

client.connect_signal("manage", function(c)
    local icon = menubar.utils.lookup_icon(c.instance)
    local lower_icon = menubar.utils.lookup_icon(c.instance:lower())

    --Check if the icon exists
    if icon ~= nil then
        c.icon = gears.surface(icon)._native

    --Check if the icon exists in the lowercase variety
    elseif lower_icon ~= nil then
        c.icon = gears.surface(lower_icon)._native

    --Check if the client already has an icon. If not, give it a default.
    elseif c.icon == nil then
        c.icon = gears.surface(menubar.utils.lookup_icon("application-default-icon"))._native
    end
end)

-- Create the Wibar -----------------------------------------------------------

screen.connect_signal("request::desktop_decoration", function(s)
    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()

    -- Create layoutbox widget
    s.mylayoutbox = awful.widget.layoutbox(s)

    -- Create the wibox
    s.mywibox = awful.wibar({position = "top", screen = s, type = "dock"})

    -- Remove wibar on full screen
    local function remove_wibar(c)
        if c.fullscreen or c.maximized then
            c.screen.mywibox.visible = false
        else
            c.screen.mywibox.visible = true
        end
    end

    -- Remove wibar on full screen
    local function add_wibar(c)
        if c.fullscreen or c.maximized then
            c.screen.mywibox.visible = true
        end
    end

    -- Hide bar when a splash widget is visible
    awesome.connect_signal("widgets::splash::visibility", function(vis)
        screen.primary.mywibox.visible = not vis
    end)

    client.connect_signal("property::fullscreen", remove_wibar)

    client.connect_signal("request::unmanage", add_wibar)

    -- Create the taglist widget
    s.mytaglist = require("ui.widgets.pacman_taglist")(s)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen = s,
        filter = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons,
        bg = beautiful.xcolor0,
        style = {bg = beautiful.xcolor0},
        layout = {
            spacing = dpi(0),
            spacing_widget = {
                bg = beautiful.xcolor8,
                widget = wibox.container.background
            },
            layout = wibox.layout.fixed.horizontal
        },
        widget_template = {
            {
                {
                    nil,
                    awful.widget.clienticon,
                    nil,
                    layout = wibox.layout.fixed.horizontal
                },
                top = dpi(5),
                bottom = dpi(5),
                left = dpi(10),
                right = dpi(10),
                widget = wibox.container.margin
            },
            id = 'background_role',
            widget = wibox.container.background
        }
    }

    local final_systray = wibox.widget {
        {mysystray_container, top = dpi(5), layout = wibox.container.margin},
        bg = beautiful.xcolor0,
        shape = helpers.rrect(beautiful.border_radius - 3),
        widget = wibox.container.background
    }

    -- Add widgets to the wibox
    s.mywibox:setup{
        layout = wibox.layout.align.vertical,
        nil,
        {
            layout = wibox.layout.align.horizontal,
            expand = "none",
            {
                layout = wibox.layout.fixed.horizontal,
                {
                    {
                        {
                            awesome_icon,
                            s.mytaglist,
                            spacing = 15,
                            spacing_widget = {
                                color = beautiful.xcolor8,
                                shape = gears.shape.powerline,
                                widget = wibox.widget.separator
                            },
                            layout = wibox.layout.fixed.horizontal
                        },
                        bg = beautiful.xcolor0,
                        shape = helpers.rrect(beautiful.border_radius - 3),
                        widget = wibox.container.background
                    },
                    top = dpi(6),
                    left = dpi(10),
                    right = dpi(5),
                    bottom = dpi(6),
                    widget = wibox.container.margin
                },
            },
            {
                {
                    {
                        s.mytasklist,
                        bg = beautiful.xcolor0 .. "00",
                        shape = helpers.rrect(beautiful.border_radius - 3),
                        widget = wibox.container.background
                    },
                    top = dpi(6),
                    left = dpi(5),
                    bottom = dpi(6),
                    right = dpi(5),
                    widget = wibox.container.margin
                },
                widget = wibox.container.constraint
            },
            {
                {
                    top = dpi(6),
                    left = dpi(5),
                    bottom = dpi(6),
                    right = dpi(5),
                    widget = wibox.container.margin
                },
                {
                    awful.widget.only_on_screen(final_systray, screen[1]),
                    top = dpi(6),
                    left = dpi(5),
                    bottom = dpi(6),
                    right = dpi(5),
                    widget = wibox.container.margin
                },
                {
                    {
                        weather_pill,
                        bg = beautiful.xcolor0,
                        shape = helpers.rrect(beautiful.border_radius - 3),
                        widget = wibox.container.background
                    },
                    top = dpi(6),
                    left = dpi(0),
                    bottom = dpi(6),
                    right = dpi(5),
                    widget = wibox.container.margin
                },
                {
                    {
                        date_pill,
                        bg = beautiful.xcolor0,
                        shape = helpers.rrect(beautiful.border_radius - 3),
                        widget = wibox.container.background
                    },
                    top = dpi(6),
                    left = dpi(5),
                    bottom = dpi(6),
                    right = dpi(5),
                    widget = wibox.container.margin
                },
                {
                    {
                        {
                            s.mylayoutbox,
                            top = dpi(4),
                            bottom = dpi(4),
                            right = dpi(7),
                            left = dpi(7),
                            widget = wibox.container.margin
                        },
                        bg = beautiful.xcolor0,
                        shape = helpers.rrect(beautiful.border_radius - 3),
                        widget = wibox.container.background
                    },
                    top = dpi(6),
                    bottom = dpi(6),
                    left = dpi(5),
                    right = dpi(10),
                    widget = wibox.container.margin
                },

                layout = wibox.layout.fixed.horizontal
            }
        },
        {
            widget = wibox.container.background,
            bg = beautiful.xcolor0,
            forced_height = beautiful.widget_border_width
        }

    }
end)

-- EOF ------------------------------------------------------------------------
