local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")

local bling = require("module.bling")

-- Set Autostart Applications
require("configuration.autostart")

-- Default Applications
terminal = "alacritty"
editor = "emacs"
editor_cmd = terminal .. " start " .. os.getenv("EDITOR")
browser = "firefox"
filemanager = "nemo"
discord = "discord"
launcher = "dmenu_run"
music = terminal .. " start --class music ncmpcpp"

-- Global Vars
screen_width = awful.screen.focused().geometry.width
screen_height = awful.screen.focused().geometry.height

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"
altkey = "Mod1"
shift = "Shift"
ctrl = "Control"

local yy = 10 + beautiful.wibar_height

-- Enable Playerctl Module from Bling
bling.signal.playerctl.enable {
    ignore = {"chromium"},
    backend = "playerctl_lib",
    update_on_activity = true
}

bling.widget.tag_preview.enable {
    show_client_content = true,
    placement_fn = function(c)
        awful.placement.top_left(c, {
            margins = {
                top = beautiful.wibar_height + beautiful.useless_gap / 2,
                left = beautiful.useless_gap / 2
            }
        })
    end,
    scale = 0.15,
    honor_padding = true,
    honor_workarea = false
}

bling.widget.task_preview.enable {
    x = 20,                       -- The x-coord of the popup
    y = 20,                       -- The y-coord of the popup
    height = 200,                 -- The height of the popup
    width = 200,                  -- The width of the popup
    placement_fn = function(c)    -- Place the widget using awful.placement (this overrides x & y)
        awful.placement.top(c, {
            margins = {
                top = beautiful.wibar_height + beautiful.useless_gap / 2
            }
        })
    end,
    honor_padding = true,
    honor_workarea = false
}

require("module.window_switcher").enable()

-- Get Keybinds
require("configuration.keys")

-- Get Rules
require("configuration.ruled")

-- Layouts and Window Stuff
require("configuration.window")

-- Scratchpad
require("configuration.scratchpad")
