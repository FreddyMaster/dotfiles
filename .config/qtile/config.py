# The following comments are the copyright and licensing information from the default
# qtile config. Copyright (c) 2010 Aldo Cortesi, 2010, 2014 dequis, 2012 Randall Ma,
# 2012-2014 Tycho Andersen, 2012 Craig Barnes, 2013 horsik, 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be included in all copies
# or substantial portions of the Software.

# IMPORTS
import os
import subprocess
from typing import List  # noqa: F401
from libqtile.config import Key, Screen, Group, Drag, Click, ScratchPad, DropDown
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook, extension, qtile
from mylib import BGroupBox
from custom.windowname import WindowName as CustomWindowName

# DEFINING SOME VARIABLES
os.environ['QT_QPA_PLATFORMTHEME'] = 'qt5ct'
MOD = "mod4"  # Sets mod key to SUPER/WINDOWS
ALT = "mod1"
MYTERM = "kitty"
MYFONT = "Hack Nerd Font"

background = "#282828"
gray       = "#4c556a"
red        = "#bf616a"
blue       = "#88c0d0"
aqua       = "#8ec07c"
white      = "#eeeeee"
focusColor = "#4c566a"
trayColor  = "#2E3430"
background = "#2E3440"
foreground = "#88C0D0"
alert = "#81A1C1"


# KEYBINDINGS
keys = [
    # The essentials
    Key([MOD], "Return",
        lazy.spawn(MYTERM),
        desc='Launches Terminal'),
    Key([MOD], "p",
        lazy.spawn("dmenu_run"),
        desc='launches dmenu'),
    Key([MOD], "space",
        lazy.next_layout(),
        desc='Toggle through layouts'),
    Key([MOD, "shift"], "c",
        lazy.window.kill(),
        desc='Kill active window'),
    Key([MOD], "q",
        lazy.restart(),
        desc='Restart Qtile'),
    Key([MOD, "shift"], "q",
        lazy.shutdown(),
        desc='Shutdown Qtile'),

    # Window controls
    Key([MOD], "j",
        lazy.layout.down(),
        desc='Move focus down in current stack pane'),
    Key([MOD], "k",
        lazy.layout.up(),
        desc='Move focus up in current stack pane'),
    Key([MOD, "shift"], "k",
        lazy.layout.shuffle_down(),
        desc='Move windows down in current stack'),
    Key([MOD, "shift"], "j",
        lazy.layout.shuffle_up(),
        desc='Move windows up in current stack'),
    Key([MOD], "h",
        lazy.layout.grow(),
        lazy.layout.increase_nmaster(),
        desc='Expand window (MonadTall), increase number in master pane (Tile)'),
    Key([MOD], "l",
        lazy.layout.shrink(),
        lazy.layout.decrease_nmaster(),
        desc='Shrink window (MonadTall), decrease number in master pane (Tile)'),
    Key([MOD], "n",
        lazy.layout.normalize(),
        desc='normalize window size ratios'),
    Key([MOD], "m",
        lazy.layout.maximize(),
        desc='toggle window between minimum and maximum sizes'),
    Key([MOD, "shift"], "f",
        lazy.window.toggle_floating(),
        desc='toggle floating'),

    # Stack controls
    Key([MOD, "shift"], "space",
        lazy.layout.rotate(),
        lazy.layout.flip(),
        desc='Switch which side main pane occupies (XmonadTall)'),

    # My applications launched with SUPER + ALT + KEY
    Key([MOD, ALT], "g",
        lazy.spawn("steam"),
        desc='Steam'),
    Key([MOD, ALT], "f",
        lazy.spawn("pcmanfm"),
        desc='File manager'),
    Key([MOD], "b",
        lazy.spawn("./.config/qtile/toggle_eww.sh"),
        desc='Toggle bottom eww bar visibility'),
    Key([MOD], "e",
        lazy.spawn("emacs"),
        desc='Opens doom emacs',
    ),

    # Volume Controls
    Key(
        [],
        "XF86AudioRaiseVolume",
        lazy.spawn("./.config/qtile/eww_vol.sh up"),
        desc="Increase volume",
    ),
    Key(
        [],
        "XF86AudioLowerVolume",
        lazy.spawn("./.config/qtile/eww_vol.sh down"),
        desc="Decrease volume",
    ),
    Key(
        [],
        "XF86AudioMute",
        lazy.spawn("./.config/qtile/eww_vol.sh mute"),
        desc="Toggle mute",
    ),
]

# GROUPS
groups = (
    Group('󰈹', layout='monadtall'),
    Group('󰙯', layout='max'),
    Group('󰇮', layout='max'),
    Group('󰮠', layout='monadtall'),
    Group('󰞷', layout='monadtall'),
    Group('󰊗', layout='max'),
)

for i, group in enumerate(groups[:-1], 1):
    # Switch to another group
    keys.append(Key([MOD], str(i), lazy.group[group.name].toscreen()))
    # Send current window to another group
    keys.append(Key([MOD, "shift"], str(i), lazy.window.togroup(group.name)))

# DEFAULT THEME SETTINGS FOR LAYOUTS
layout_theme = {"border_width": 2,
                "margin": 5,
                "border_focus": blue,
                "border_normal": focusColor
                }

# THE LAYOUTS
layouts = (
    # layout.Bsp(**layout_theme),
    # layout.Stack(stacks=2, **layout_theme),
    # layout.Columns(**layout_theme),
    # layout.RatioTile(**layout_theme),
    # layout.VerticalTile(**layout_theme),
    # layout.Matrix(**layout_theme),
    # layout.Zoomy(**layout_theme),
    #  layout.Tile(shift_windows=True, **layout_theme),
    #  layout.Stack(num_stacks=2),
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.Max(**layout_theme),
    #  layout.Floating(**layout_theme)
)

# DEFAULT WIDGET SETTINGS
widget_defaults = dict(
    font=MYFONT,
    fontsize=16,
    padding=3,
    background=background
)
extension_defaults = widget_defaults.copy()


# WIDGETS
def init_wide_bar(tray=True):
    return (
        widget.TextBox(
            text="",
            foreground=white,
            background=background,
            fontsize=28,
            padding=10,
            mouse_callbacks={
                'Button1': lambda: qtile.cmd_spawn([
                    '/bin/sh', os.path.expanduser(os.path.join(
                        '~', '.config',  'xmenu', 'xmenu.sh'))
                ])
            }
        ),
        widget.Sep(
            linewidth = 0,
            padding = 6,
            foreground = foreground,
            background = background,
        ),
        widget.GroupBox(
            font=MYFONT,
            fontsize=20,
            spacing=3,
            disable_drag=True,
            active=blue,
            inactive=gray,
            rounded=False,
            highlight_method="line",
            #this_screen_border=blue,
            highlight_color = background,
            other_current_screen_border=gray,
            other_screen_border=gray,
        ),
        widget.TextBox(
            text="-",
            foreground=foreground,
            background=background,
            padding=5,
        ),
        CustomWindowName(
            background=background,
            foreground=foreground,
            empty_group_string="Desktop",
            max_chars=40,
        ),
        widget.TextBox(
            text="󰕾",
            foreground=foreground,
            background=background,
            mouse_callbacks={'Button1': lambda: qtile.cmd_spawn('pavucontrol')}
        ),
        widget.Volume(
            foreground=foreground,
            background=background,
        ),
        widget.TextBox(
            text="|",
            foreground=foreground,
            background=background,
            padding=5,
        ),
        widget.CurrentLayoutIcon(
            custom_icon_paths=[os.path.expanduser(
                "$HOME/.config/qtile/icons")],
            foreground=foreground,
            background=background,
            scale=0.65
        ),
        widget.CurrentLayout(
            foreground=foreground,
            background=background,
            padding=5
        ),
        widget.TextBox(
            text="|",
            foreground=foreground,
            background=background,
            padding=5,
        ),
        widget.Clock(
            foreground=foreground,
            background=background,
            format="󰥔 %I:%M %P"
        ),
        widget.Sep(
            linewidth = 0,
            padding = 6,
            foreground = foreground,
            background = background,
        ),
        widget.Systray(
            background=trayColor,
            padding=5
        ) if tray else widget.Sep(linewidth=0),
        widget.Sep(
            linewidth=0,
            padding=5,
            background=background
        ),
    )


def init_short_bar():
    return (
        BGroupBox(
            font=MYFONT,
            fontsize=20,
            borderwidth=0,
            foreground=foreground,
            this_current_screen_border=background,
            this_screen_border=focusColor,
        ),
        widget.Sep(
            linewidth=0,
            padding=5,
            background=background
        ),
        widget.TaskList(
        ),
        widget.CurrentLayoutIcon(
            custom_icon_paths=[os.path.expanduser(
                "$HOME/.config/qtile/icons")],
            foreground=foreground,
            background=background,
            scale=0.7
        ),
        widget.CurrentLayout(
            foreground=foreground,
            background=background,
            padding=5
        ),
        widget.Clock(
            foreground=foreground,
            background=background,
            padding=5,
            format="%A, %d %B [ %H:%M ]"
        ),
        widget.Sep(
            linewidth=0,
            padding=5,
            background=background
        )
    )


bar_list = (
    init_wide_bar(),
    init_wide_bar(tray=False),
    init_short_bar(),
)

if __name__ in ["config", "__main__"]:
    screens = []
    for widgets in bar_list:
        screens.append(Screen(top=bar.Bar(widgets=widgets, opacity=0.95, size=28)))

##### DRAG FLOATING WINDOWS #####
mouse = (
    Drag([MOD], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([MOD], "Button2", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([MOD], "Button3", lazy.window.bring_to_front())
)

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

# FLOATING WINDOWS
floating_layout = layout.Floating(float_rules=[
    *layout.Floating.default_float_rules
], **layout_theme)

auto_fullscreen = True
focus_on_window_activation = "smart"


# STARTUP APPLICATIONS
@hook.subscribe.startup
def runner():
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
