#!/usr/bin/env bash

picom --config ~/.config/picom/picom.conf &
nitrogen --restore &
xclickroot -r sh ~/.config/xmenu/xmenu.sh &
flameshot &
udiskie -a -N -t &
gammy &
mpd &
/usr/bin/emacs --daemon &
kitty -e ncmpcpp &
mpnotd --config ~/.config/mpnotd/mpnotdrc --time 2 &
nm-applet &
com.discordapp.Discord &
kunst --music_dir ~/Music --size 150x150 --silent &
eww daemon &
sleep 2
eww open border &
eww open border1 &
eww open border2 &
./.config/eww/scripts/getweather &
