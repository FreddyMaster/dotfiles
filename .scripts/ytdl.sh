#!/usr/bin/env bash

yt-dlp $1 | rg 'Merging formats into "/home/aj/Videos/' | sed 's/.*"\(.*\)"[^"]*$/\1/' | xclip -selection clipboard

echo "Copied"
