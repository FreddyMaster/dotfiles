#!/usr/bin/env bash

INFILE=$1
START=$2
STOP=$3
OUTFILE=$4

ffmpeg -ss "$START" -to "$STOP" -i "$INFILE" "$OUTFILE"
