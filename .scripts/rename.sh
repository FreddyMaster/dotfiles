#!/usr/bin/env bash

FILE="*.mkv"
FILETRIMMED=$(echo "$FILE" | cut -f 1 -d '.' | rev | cut -c4- | rev)
echo "$FILETRIMMED"
# Rename all *.txt to *.text
for f in '$FILETRIMMED"'; do
    mv -- "$f" "${f%.mkv}.mp4"
done
