
str="$(pacmd list-sink-inputs)"

list1=$(printf '%s' "$str" | grep 'index: ')
list2=$(printf '%s' "$str" | grep 'application.process.binary = "')

arr1=()
arr2=()

while read line; do
    arr1+=("${line:7}")
done <<< "$list1"

while read line; do
    arr2+=("${line:30:-1}")
done <<< "$list2"

for index in ${!arr1[@]}; do
    s1="${arr1[index]}"
    s2="${arr2[index]}"

    echo "$s2 | $s1"
done
