#!/usr/bin/env bash

# Utility to convert video files to GIFs using ffmpeg
#
# Usage: convert-to-gif.sh <video-file-path>
# To skip frames: convert-to-gif.sh <video-file-path> <time-in-seconds>
# Example:
# convert-to-gif.sh video.mp4 28


if [[ -z "$1" ]]; then
    echo "No video file specified"
    exit 1
fi

# get everything after last /
video=${1##*/}
# remove everything after .
filename=${video%.*}

echo -e "$(tput bold)Generating Palette $(tput sgr0)"
# Generate palette
ffmpeg -i "$1" -vf "fps=22,scale=320:-1:flags=lanczos,palettegen" "$filename".png

echo -e "$(tput bold)Converting Video to GIF $(tput sgr0)"

if [[ "$2" ]]; then
    ffmpeg -t "$2" -i "$1" -i "$filename".png -filter_complex "fps=22,scale=320:-1:flags=lanczos[x];[x][1:v]paletteuse" "$filename".gif
else
    ffmpeg -i "$1" -i "$filename".png -filter_complex "fps=22,scale=320:-1:flags=lanczos[x];[x][1:v]paletteuse" "$filename".gif
fi

echo -e "Removing palette"
rm "$filename".png
